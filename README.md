#k8s 搭建httpbin 
## 1，安装docker
###
   查看是已经安装
   rpm -qa|grep docke
   如未安装-下载依赖
   yum install -y yum-utils device-mapper-persistent-data lvm2
   yum -y install wget
   下载repo文件，设置yum源（根据自己系统的发行版，有些系统可直接安装docker）
   wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo
   清除yum换成
   yum makecache fast
   安装docker
   yum install docker-ce
   启动，开机自启动
   systemctl start docker
   systemctl enable docker
   验证
   docker version
## 2，安装docker私有镜像仓库
###
   下载registry私有仓库镜像
   docker pull registry
   配置私有仓库地址
   vim /etc/docker/daemon.json
   "insecure-registries": ["本机ip 10.130.17.125:5000"]
   重启docker
   systemctl restart docker
   运行registry
   docker run -d -v /opt/registry:/var/lib/registry -p 5000:5000 --name registry docker.io/registry
## 3，搭建k8s   
### （本人使用的是青云研发的产品一键安装k8s 链接：https://kubesphere.com.cn/docs/quick-start/all-in-one-on-linux/）
   下载安装工具
   curl -sfL https://get-kk.kubesphere.io | VERSION=v1.1.0 sh -
   开始安装
   ./kk create cluster --with-kubernetes v1.20.4 --with-kubesphere v3.1.0
   验证
   kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
   
## 4，下载httpbin镜像
### 
   docker pull kennethreitz/httpbin
   推送镜像到私有仓库
   docker tag kennethreitz/httpbin 127.0.0.1:5000/httpbin
   docker push 127.0.0.1:5000/httpbin
## 5，k8s 搭建httbin
### 
   编写httbinpod文件
vim httpbinpod
apiVersion: apps/v1
kind: Deployment
metadata:
 creationTimestamp: null
 labels:
  app: httpbin
 name: httpbin
spec：
 replicas: 2
 selector:
  matchLabels:
   app: httpbin
 strategy: {}
 template:
  metadata:
   creationTimestamp: null
   labels:
   app: httpbin
  spec：
   containers:
   - image: 127.0.0.1:5000/httpbin
   name: httpbin
   imagePullPolicy: Never
   ports:
	- containerPort: 80
---
apiVersion: v1
kind: Service													    
metadata:
 name: httbin-service
spec:
 ports:														    
 - port: 80														   targetPort: 80												
   protocol: TCP
 selector:								
 name: httpbin
### 
  编写httpbin-ingress-ssl.yaml
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
   name: httpbin-ingress
   namespace: default
   annotations:
   kubernetes.io/ingress.class: "127.0.0.1:5000/httpbi"
spec:
 rules:
 - host: whh.cn
 http:
  paths:
  - path:
     backend:
      serviceName: httpbin-ingress
      servicePort: 80
  tls:
    - hosts:
      - whh.cn
    secretName: httpbin-test

## 6，创建证书
### 
   openssl genrsa -out tls.key 2048
   openssl req -new -x509 -key tls.key -out tls.crt -subj /C=CN/ST=Guangdong/L=Guangzhou/O=devops/CN=whh.cn
   kubectl create secret tls httpbin-test --cert=tls.crt --key=tls.key
   kubectl get secret
## 7，启动httbin
### 
   kubectl apply -f httpbinpod.yaml
   kubectl apply -f http-ingress-ssl.yaml

